module gitlab.com/farkhutdinov/simple-crud-rest-api-with-db-postgres

go 1.15

require (
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-pg/pg/v10 v10.7.5
)
