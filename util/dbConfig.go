package util

import (
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/farkhutdinov/simple-crud-rest-api-with-db-postgres/data"
	"log"
)

func NewDBConn() (con *pg.DB) {

	address := fmt.Sprintf("%s:%s", "localhost", "5432")
	options := &pg.Options{
		User:     "postgres",
		Password: "postgres",
		Addr:     address,
		Database: "go_db",
		PoolSize: 50,
	}
	con = pg.Connect(options)

	if con == nil {
		log.Print("cannot connect to postgres")
	}
	return
}

func CreateSchema(db *pg.DB) error {
	models := []interface{}{
		(*data.User)(nil),
		(*data.Story)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp: true,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
