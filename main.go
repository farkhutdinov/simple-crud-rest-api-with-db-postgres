package main

import (
	"fmt"
	"gitlab.com/farkhutdinov/simple-crud-rest-api-with-db-postgres/data"
	"gitlab.com/farkhutdinov/simple-crud-rest-api-with-db-postgres/util"
	"log"
)

func main() {
	db := util.NewDBConn()

	defer db.Close()

	err := util.CreateSchema(db)
	if err != nil {
		panic(err)
	}

	user1 := &data.User{
		Name:   "admin",
		Emails: []string{"admin1@admin", "admin2@admin"},
	}
	_, err = db.Model(user1).Insert()
	if err != nil {
		panic(err)
	}

	_, err = db.Model(&data.User{
		Name:   "root",
		Emails: []string{"root1@root", "root2@root"},
	}).Insert()
	if err != nil {
		panic(err)
	}

	story1 := &data.Story{
		Title:    "Cool story",
		AuthorId: user1.Id,
	}
	_, err = db.Model(story1).Insert()
	if err != nil {
		panic(err)
	}

	// Select user by primary key.
	user := &data.User{Id: user1.Id}
	err = db.Model(user).WherePK().Select()
	if err != nil {
		panic(err)
	}

	//Select all users.
	//TODO тут какая-то ошибка
	var users []data.User
	err = db.Model(users).Select()
	if err != nil {
		log.Print(err.Error() + "cannot connect to postgres")
	}

	// Select story and associated author in one query.
	story := new(data.Story)
	err = db.Model(story).
		Relation("Author").
		Where("story.id = ?", story1.Id).
		Select()
	if err != nil {
		panic(err)
	}

	fmt.Println(user)
	fmt.Println(users)
	fmt.Println(story)
}
