package data

import (
	"fmt"
)

type Story struct {
	Id       int64
	Title    string
	AuthorId int64
	Author   *User `pg:"rel:has-one"`
}

func (s Story) String() string {
	return fmt.Sprintf("{Story:%d %s %s}", s.Id, s.Title, s.Author)
}
